# README
编译环境：zhimage/golang:1.17.8-sonarqube

## BUILD
```shell
go mod tidy
go build -o cicd-demo-server main.go
```

## Unit Test
```shell
cd utils
go test -v
```

## code analysis
```shell
go get github.com/golang/lint
go install github.com/golang/lint

golint utils    //Why fail???
```

## build docker image
```shell
docker build -t zhimage/cicd-demo-server:0.1 .   
```

## tutorial
1. [GitLab CI CD Tutorial for Beginners - Crash Course](https://www.youtube.com/watch?v=qP8kir2GUgo)
2. [GitLab CI/CD Tutorial For Beginners | Learn GitLab In 1 Hour](https://www.youtube.com/watch?v=B68jcGfH4C8)
3. [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/)